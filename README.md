# Transition Laboratory of Godot

![](screenshot.png)

# Online
# https://monnef.gitlab.io/transition_laboratory_of_godot

# Download
Exported versions for Linux, Mac and Windows are in this repository in directory `build`. (I can only test Linux version which works fine.)

To download a release version go to [tags](https://gitlab.com/monnef/transition_laboratory_of_godot/-/tags) and click on a download button 📥.

To download the latest version click on the download button 📥 (besides the blue Clone button) at the top of the project details page and select zip (or other archive format).

# Alternatives
* [cheat sheet](https://raw.githubusercontent.com/godotengine/godot-docs/master/img/tween_cheatsheet.png)
* [easings.net](https://easings.net/)
