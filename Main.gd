extends Node2D


onready var _container:= $Bg/GridContainer
onready var _easing:= $Bg/Form/Easing
onready var _duration:= $Bg/Form/CenterContainer/DurationSlider
onready var _duration_label:= $Bg/Form/DurationLabel
onready var _fps_label:= $Bg/Form/FpsLabel
onready var _vsync_checkbox:= $Bg/Form/VsyncCheckBox
onready var _and_back_checkbox:= $Bg/Form/AndBackCheckBox
onready var _invert_checkbox:= $Bg/Form/InvertCheckBox

var _tile:= preload("TransitionTile.tscn")

func _ready():
	for x in Constants.easing_names: _easing.add_item(x)
	_refresh_duration_label()
	_vsync_checkbox.pressed = OS.vsync_enabled
	for x in range(Constants.transition_names.size()):
		var i:= _tile.instance()
		i.transition_type = x
		_container.add_child(i)
	_refresh_all_children()

func _process(delta: float) -> void:
	_fps_label.text = str(Engine.get_frames_per_second())

func _refresh_all_children(delayed: bool = false) -> void:
	for x in _container.get_children():
		x.easing = _easing.selected
		x.anim_duration = _duration.value
		x.forth_and_back = _and_back_checkbox.pressed
		x.invert = _invert_checkbox.pressed
		x.refresh()
		if delayed: yield(get_tree().create_timer(0.1), "timeout")

func _refresh_duration_label() -> void:
	_duration_label.text = "%ss" % [_duration.value]

func _on_Easing_item_selected(id: int) -> void:
	_refresh_all_children()

func _on_DurationSlider_value_changed(value: float) -> void:
	_refresh_duration_label()
	_refresh_all_children()

func _on_ResetTilesButton_pressed() -> void:
	_refresh_all_children()

func _on_VsyncCheckBox_pressed() -> void:
	OS.vsync_enabled = _vsync_checkbox.pressed

func _on_AndBackCheckBox_pressed() -> void:
	_refresh_all_children()

func _on_InvertCheckBox_pressed() -> void:
	_refresh_all_children()

func _on_SrcButton_pressed() -> void:
	OS.shell_open(Constants.project_url)

func _on_TweenDocsButton_pressed() -> void:
	OS.shell_open(Constants.tween_docs_url)
