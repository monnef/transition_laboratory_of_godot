extends Resource

class_name Constants

const transition_names:= [
	"Linear",
	"Sine",
	"Quint",
	"Quart",
	"Quad",
	"Expo",
	"Elastic",
	"Cubic",
	"Circ",
	"Bounce",
	"Back"
]

const easing_names:= ["In", "Out", "In Out", "Out In"]

const project_url:= "https://gitlab.com/monnef/transition_laboratory_of_godot"

const tween_docs_url:= "https://docs.godotengine.org/en/stable/classes/class_tween.html"
