extends Panel

export var transition_type:= 0
export var easing:= 0
export var anim_duration:= 3.0
export var forth_and_back:= false
export var invert:= false

onready var _show_tween:= $ShowTween

onready var _chart:= $ChartWrapper/Chart
onready var _chart_bg:= $ChartWrapper/Bg
onready var _jumpy_sprite:= $JumpyWrapper/ColorRect/Sprite
onready var _rotation_sprite:= $RotationWrapper/ColorRect/Sprite
onready var _scale_sprite:= $ScaleWrapper/ColorRect/Sprite
onready var _fade_sprite:= $FadeWrapper/ColorRect/Sprite

onready var _chart_line:= $ChartWrapper/Bg/ChartLine
onready var _chart_one_line:= $ChartWrapper/Bg/OneLine
onready var _chart_zero_line:= $ChartWrapper/Bg/ZeroLine
onready var _chart_time_line:= $ChartWrapper/Bg/TimeLine
onready var _trans_type_label:= $TransTypeLabel
onready var _border:= $ChartWrapper/Border

var _capturing:= false

const capture_min_x_step:= 0.5

func _ready() -> void:
	_init_lines()
	_setup_border()
	refresh()

func _setup_border() -> void:
	var s:= 4.0
	var hs:= s / 2.0
	_border.rect_size = _chart_bg.rect_size + Vector2(s, s)
	_border.rect_position = _chart_bg.rect_position - Vector2(hs, hs)

func _get_total_anim_duration() -> float:
	if forth_and_back: return anim_duration * 2
	else: return anim_duration

func _init_lines() -> void:
	var overflow_fix:= -0.0035
	_chart_line.clear_points()
	_chart_zero_line.position = _chart_line.position
	_chart_zero_line.points[0] = _calc_point_pos(0, 0)
	_chart_zero_line.points[1] = _calc_point_pos(_get_total_anim_duration() + overflow_fix, 0)
	_chart_one_line.position = _chart_line.position
	_chart_one_line.points[0] = _calc_point_pos(0, 1)
	_chart_one_line.points[1] = _calc_point_pos(_get_total_anim_duration() + overflow_fix, 1)
	_chart_time_line.position = _chart_line.position
	_chart_time_line.points[0] = _calc_point_pos(0, -0.42)
	_chart_time_line.points[1] = _calc_point_pos(0, 1.42)

func refresh():
	_trans_type_label.text = Constants.transition_names[transition_type]
	_chart_line.clear_points()
	_capturing = true
	_setup_animation()

func _setup_animation() -> void:
	_show_tween.remove_all()
	var start_value:= 0.0
	var stop_value:= 1.0
	if invert:
		start_value = 1
		stop_value = 0
	_show_tween.interpolate_method(self, "_set_show_data", start_value, stop_value, anim_duration, transition_type, easing)
	if forth_and_back:
		_show_tween.interpolate_method(self, "_set_show_data", stop_value, start_value, anim_duration, transition_type, easing, anim_duration)
	_show_tween.start()

func _get_x_axis_offset() -> float: return _get_chart_size().y * 0.145

func _get_value_mult() -> float: return _get_x_axis_offset() * 3.75

func _get_pos_mul() -> float: return float(_get_chart_size().x) / _get_total_anim_duration()

func _calc_point_pos(x: float, y: float) -> Vector2: return Vector2(x * _get_pos_mul(), - (_get_x_axis_offset() + y * _get_value_mult()))

func  _set_show_data(value: float) -> void:
	var jump_start_y:= 221
	var jump_stop_y:= 27
	_jumpy_sprite.position.y = jump_start_y + (jump_stop_y - jump_start_y) * value
	_rotation_sprite.rotation_degrees = 360 * value
	_scale_sprite.scale = Vector2.ONE * value * 0.15
	_fade_sprite.modulate.a = value
	var time = _show_tween.tell()
	_chart_time_line.position.x = _get_chart_size().x * time / _get_total_anim_duration()
	if _capturing:
		var point_pos:= _calc_point_pos(time, value)
		if _chart_line.points.empty() || abs(_chart_line.points[_chart_line.points.size() - 1].x - point_pos.x) > capture_min_x_step:
			_chart_line.add_point(point_pos)

func _on_ShowTween_tween_all_completed() -> void:
	if _capturing: _capturing = false
	_setup_animation()

func _get_chart_size() -> Vector2: return _chart_bg.rect_size
